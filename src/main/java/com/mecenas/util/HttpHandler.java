package com.mecenas.util;

import com.mecenas.AlchemyResult;
import com.mecenas.GoogleTrend;
import com.mecenas.WebCrawlerResult;
import org.apache.commons.httpclient.DefaultHttpMethodRetryHandler;
import org.apache.commons.httpclient.DefaultMethodRetryHandler;
import org.apache.commons.httpclient.params.HttpMethodParams;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.http.HttpEntity;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.DefaultHttpRequestRetryHandler;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.HTTP;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.*;
import java.net.URI;
import java.net.URISyntaxException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

/**
 * Created by admin on 23/11/16.
 */
public class HttpHandler {


    private static final String crawlerAddress = "https://mecenas-web-crawler.eu-gb.mybluemix.net/queryDomain";
//    private static final String crawlerAddress = "http://localhost:3000/queryDomain";
    private static final String googleTrendAddress = "http://localhost:3000/googleTrends/";
    private static final String alchemyAPIKey = "22f09a1547d1ec71ec736590b6aba60e6b680c92";
    private static final String alchemyAddress = "https://gateway-a.watsonplatform.net/calls/url/URLGetCombinedData?apikey=" + alchemyAPIKey;
    CloseableHttpClient httpclient;

    private static boolean alchemyLimit = false;
    private static Date alchemyLimitDate;

    public HttpHandler(){
        httpclient = HttpClients.custom().setRetryHandler(new DefaultHttpRequestRetryHandler(10, true)).build();
    }

    public WebCrawlerResult getWebCrawlerResult(String link, String keyword){
        HttpGet httpGet = new HttpGet(crawlerAddress);
        final RequestConfig params = RequestConfig.custom().setConnectTimeout(60000).setSocketTimeout(60000).build();
        httpGet.setConfig(params);
        try {

            URI uri = new URIBuilder(httpGet.getURI()).addParameter("domain", link).addParameter("keyword", keyword).build();
            httpGet.setURI(uri);
            CloseableHttpResponse response = httpclient.execute(httpGet);

            HttpEntity entity = response.getEntity();

            if (entity != null) {
                InputStream instream = entity.getContent();
                String result = convertStreamToString(instream);
                instream.close();
                WebCrawlerResult report = new WebCrawlerResult(new JSONArray(result), link);
                System.out.println(report.getLinksSize());
                return report;
            }

        } catch (IOException e) {
            e.printStackTrace();
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }

        return null;
    }

   public GoogleTrend getGoogleTrendResult(String keyword){
        HttpGet httpGet = new HttpGet(googleTrendAddress + keyword);
        final RequestConfig params = RequestConfig.custom().setConnectTimeout(60000).setSocketTimeout(60000).build();
        httpGet.setConfig(params);
        try {
            CloseableHttpResponse response = httpclient.execute(httpGet);

            HttpEntity entity = response.getEntity();

            if (entity != null) {
                InputStream instream = entity.getContent();
                String result = convertStreamToString(instream);
                instream.close();
                GoogleTrend trend = new GoogleTrend(result);
                return trend;
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }


    public AlchemyResult getAlchemyResult(String link, String searchKeyword){
        File f = new File("alchemyReports/" + link.replace("/", "").replace(".", ""));
        if(f.exists()) {
            FileInputStream fisTargetFile = null;
            try {
                fisTargetFile = new FileInputStream(f);
                String targetFileStr = IOUtils.toString(fisTargetFile, "UTF-8");
                if(!new JSONObject(targetFileStr).getString("status").equals("OK"))
                    return null;
                return new AlchemyResult(targetFileStr, searchKeyword);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        else {
            if(alchemyLimit) {
                SimpleDateFormat fmt = new SimpleDateFormat("yyyyMMdd");
                if(fmt.format(alchemyLimitDate).equals(fmt.format(new Date())))
                    return null;
                else{
                    System.out.println("Alchemy limit was set to reached but new day, gonna reset it");
                    alchemyLimit = false;
                    return getAlchemyResultWork(link, searchKeyword);
                }
            }
            else {
                return getAlchemyResultWork(link, searchKeyword);
            }
        }
        return null;
    }

    private AlchemyResult getAlchemyResultWork(String link, String searchKeyword) {

        HttpPost post = new HttpPost(alchemyAddress);

        List<NameValuePair> nvps = new ArrayList <NameValuePair>();
        nvps.add(new BasicNameValuePair("outputMode", "json"));
        nvps.add(new BasicNameValuePair("extract", "authors,entities,keywords,title,pub-date"));
        nvps.add(new BasicNameValuePair("sentiment", "1"));
        nvps.add(new BasicNameValuePair("url", link));

        final RequestConfig params = RequestConfig.custom().setConnectTimeout(500000).setSocketTimeout(500000).build();
        post.setConfig(params);
        try {
            post.setEntity(new UrlEncodedFormEntity(nvps, HTTP.UTF_8));
            CloseableHttpResponse response = httpclient.execute(post);

            HttpEntity entity = response.getEntity();

            if (entity != null) {
                InputStream instream = entity.getContent();
                String result = convertStreamToString(instream);
                JSONObject obj = new JSONObject(result);
                if(!obj.getString("status").equals("OK")){
                    if(obj.getString("statusInfo").equals("daily-transaction-limit-exceeded")){
                        System.out.println("Alchemylanguage limit exceeded for the day");
                        alchemyLimit = true;
                        alchemyLimitDate = new Date();
                    }
                    return null;
                }
                File file = new File("alchemyReports/" + link.replace("/", "").replace(".", ""));
                FileUtils.writeStringToFile(file, result);
                instream.close();

                AlchemyResult alchemy = new AlchemyResult(obj, searchKeyword);
                return alchemy;

            }

        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    private static String convertStreamToString(InputStream is) {

        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();

        String line = null;
        try {
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                is.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return sb.toString();
    }

}
