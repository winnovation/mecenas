package com.mecenas.util.db;

import com.mecenas.dataobjects.Source;

import java.sql.*;
import java.util.ArrayList;

/**
 * Created by admin on 23/11/16.
 */
public class DBAccess {


    private static final String connUrl = "jdbc:postgresql://sl-eu-lon-2-portal.2.dblayer.com:15650/mecenas";
    private static final String dbUser = "admin";
    private static final String dbPassword = "WACJFZTJAYBSLXXK";

    Connection conn;
    public DBAccess(){

    }

    public void connect(){
        try {
            Class.forName("org.postgresql.Driver");
            conn = DriverManager
                    .getConnection(connUrl,
                            dbUser, dbPassword);
        } catch (Exception e) {
            e.printStackTrace();
            System.err.println(e.getClass().getName()+": "+e.getMessage());
            System.exit(0);
        }
    }

    public ArrayList<Source> getSources(){
        ArrayList<Source> sources = new ArrayList<Source>();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery( "SELECT * FROM source;" );
            while ( rs.next() ) {
                String url = rs.getString("url");
                double  influence = rs.getDouble("influence");
                sources.add(new Source(url, influence));
                System.out.println( "url = " + url );
                System.out.println( "influence = " + influence);
            }
            rs.close();
            stmt.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return sources;

    }
}
