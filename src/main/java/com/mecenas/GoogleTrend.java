package com.mecenas;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by admin on 05/12/16.
 */
public class GoogleTrend {

    private ArrayList<GoogleTrendValue> values;

    public GoogleTrend(String obj) {
        this(new JSONArray(obj));
    }

    public GoogleTrend(JSONArray originalArray) {
        this.values = new ArrayList<GoogleTrendValue>();
        if(originalArray != null){
            JSONArray array = originalArray.optJSONObject(0).optJSONArray("values");
            if(array != null){
                for(int i = 0; i < array.length(); i++)
                    this.values.add(new GoogleTrendValue(array.optJSONObject(i)));
            }
        }
    }

    public ArrayList<GoogleTrendValue> getValues(){
        return this.values;
    }




    public class GoogleTrendValue implements Comparable {

        private Date date;
        private int value;

        private SimpleDateFormat x = new SimpleDateFormat("yyyy'-'MM'-'dd'T'HH':'mm':'ss");

        public GoogleTrendValue(JSONObject obj) {
            parseDate(obj.optString("date"));
            value = obj.optInt("value");
        }
        private void parseDate(String date) {
            if(date != null){
                try {
                    this.date = x.parse(date);
                } catch (ParseException e) {
                }
            }
        }


        public Date getDate() {
            return date;
        }
        public int getValue() {
            return value;
        }

        public int compareTo(Object r) {
            if(this.date != null && ((GoogleTrendValue)r).getDate() != null)
                return this.date.compareTo(((GoogleTrendValue)r).getDate());
            else
                return 1;
        }
    }
}


