package com.mecenas;

import com.mecenas.util.HttpHandler;
import java.util.*;

/**
 * Created by admin on 23/11/16.
 */
public class AlchemyReport {

    private LinkedList<AlchemyResult> results;
    private LinkedList<AlchemyResult> firstTermResults;     //Oldest articles
    private LinkedList<AlchemyResult> secondTermResults;
    private LinkedList<AlchemyResult> thirdTermResults;
    private LinkedList<AlchemyResult> fourthTermResults;    //Recent articles
    private HttpHandler http;

    public AlchemyReport(List<String> links, final String searchKeyword){
        http = new HttpHandler();
        results = new LinkedList<>();

        final AlchemyReport r = this;
        ArrayList<Thread> threads = new ArrayList<>();
        for(final String l : links){
            final Thread t = new Thread(new Runnable() {
                public void run() {
                    /*try {
                        Thread.sleep(100);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }*/
                    AlchemyResult result = http.getAlchemyResult(l, searchKeyword);
                    synchronized (r){
                        if(result != null)
                            results.add(result);
                    }
                }
            });
            threads.add(t);
            t.run();
        }
        try {
            for(Thread t : threads)
                t.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        Collections.sort(results);
    }

    public void sortResultsTerms(Coordinator.DateMargin dateMargin){
        firstTermResults = new LinkedList<>();
        secondTermResults = new LinkedList<>();
        thirdTermResults = new LinkedList<>();
        fourthTermResults = new LinkedList<>();

        for(AlchemyResult r : results){
           if(r.getPublicationDate().before(dateMargin.getLongMidTerm()))
               firstTermResults.add(r);
           else if(r.getPublicationDate().before(dateMargin.getMidTerm()))
               secondTermResults.add(r);
           else if(r.getPublicationDate().before(dateMargin.getShortTerm()))
               thirdTermResults.add(r);
           else
               fourthTermResults.add(r);
        }


        getAverageSentiment(getFirstTermResults());
        getAverageSentiment(getSecondTermResults());
        getAverageSentiment(getThirdTermResults());
        getAverageSentiment(getFourthTermResults());


    }


    public double getFourthTermMentionNumber(){
        return fourthTermResults.size();
    }
    public double getFourthTermSentimentNumber(){
        return getAverageSentiment(fourthTermResults);
    }


    public double getThirdTermMentionNumber(){
        return thirdTermResults.size();
    }
    public double getThirdTermSentimentNumber(){
        return getAverageSentiment(thirdTermResults);
    }



    public double getSecondTermMentionNumber(){
        return secondTermResults.size();
    }
    public double getSecondTermSentimentNumber(){
        return getAverageSentiment(secondTermResults);
    }
    public double getLongMidTermMentionGrow(){
        int firstTermSize = firstTermResults.size();
        int secondTermSize = secondTermResults.size();
        int thirdTermSize = thirdTermResults.size();
        int fourthTermSize = fourthTermResults.size();

        return ((((secondTermSize + thirdTermSize + fourthTermSize)/3.0) - firstTermSize) / firstTermSize) * 100;
    }



    public int getFirstTermMentionNumber() {
        return firstTermResults.size();
    }

    public double getFirstTermSentimentValue(){
        return getAverageSentiment(firstTermResults);
    }



    public Date getFirstValidDate(){
        for(AlchemyResult r : results)
            if(r.getPublicationDate() != null)
                return r.getPublicationDate();
        return new Date();
    }

    public LinkedList<AlchemyResult> getFirstTermResults(){
        return firstTermResults;
    }
    public LinkedList<AlchemyResult> getSecondTermResults(){
        return secondTermResults;
    }
    public LinkedList<AlchemyResult> getThirdTermResults(){
        return thirdTermResults;
    }
    public LinkedList<AlchemyResult> getFourthTermResults(){
        return fourthTermResults;
    }

    private static double getAverageSentiment(List<AlchemyResult> results){
        if(results.size() == 0)
            return 0;
        double sentiment = 0;
        double numberValidSentiments = 0;
        for(AlchemyResult r: results){
            if(r.hasRelatedEntity()){
                numberValidSentiments ++;
                sentiment += r.getOverallSentiment();
            }
        }
        if(sentiment == 0)
            return 0;
        return sentiment / numberValidSentiments;
    }
}

//Relevance of results should be ordered according to number of mentions of each one
