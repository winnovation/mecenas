package com.mecenas.dataobjects;

import org.json.JSONObject;

/**
 * Created by admin on 22/11/16.
 */
public class Keyword {

    private double relevance;
    private Sentiment sentiment;
    private String text;

    public Keyword(JSONObject obj) {
        relevance = obj.getDouble("relevance");
        sentiment = new Sentiment(obj.getJSONObject("sentiment"));
        text = obj.getString("text");
    }

}
