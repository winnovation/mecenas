package com.mecenas.dataobjects;

/**
 * Created by admin on 23/11/16.
 */
public class Source {
    private String url;
    private double influence;

    public Source(String url, double influence){
        this.url = url;
        this.influence = influence;
    }


    public double getInfluence() {
        return influence;
    }

    public String getUrl() {
        return url;
    }
}
