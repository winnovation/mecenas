package com.mecenas.dataobjects;

import org.json.JSONObject;

/**
 * Created by admin on 22/11/16.
 */
public class Sentiment {

    private SentimentType type;
    private double score;
    private double mixed;

    public Sentiment(JSONObject sentiment) {
        setSentimentType(sentiment.getString("type"));
//        When its neutral there is no score
        if(type == SentimentType.NEUTRAL)
            score = 0;
        else
            score = sentiment.getDouble("score");
        mixed = sentiment.optDouble("mixed");
    }

    private void setSentimentType(String sentimentType) {
        if(sentimentType.equals("positive"))
            type = SentimentType.POSITIVE;
        else if(sentimentType.equals("negative"))
            type = SentimentType.NEGATIVE;
        else
            type = SentimentType.NEUTRAL;
    }

    public double getScore() {
        return score;
    }

    public double getMixed() {
        return mixed;
    }

    public SentimentType getType() {
        return type;
    }
}
