package com.mecenas.dataobjects;

import org.json.JSONObject;

/**
 * Created by admin on 22/11/16.
 */
public class DocEmotions {
    private double anger;
    private double disgust;
    private double fear;
    private double joy;
    private double sadness;


    public DocEmotions(JSONObject obj) {
        if(obj != null){
            this.anger = obj.optDouble("anger");
            this.anger = obj.optDouble("disgust");
            this.anger = obj.optDouble("fear");
            this.anger = obj.optDouble("joy");
            this.anger = obj.optDouble("sadness");
        }
    }

    public double getAnger() {
        return anger;
    }

    public double getDisgust() {
        return disgust;
    }

    public double getFear() {
        return fear;
    }

    public double getJoy() {
        return joy;
    }

    public double getSadness() {
        return sadness;
    }
}
