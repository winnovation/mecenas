package com.mecenas.dataobjects;

import org.json.JSONObject;

/**
 * Created by admin on 22/11/16.
 */
public class Entity {

    private String type;
    private double relevance;
    private Sentiment sentiment;
    private int count;
//    private Disambiguated ....
    private String text;
    private String name;
    private String dbpedia;

    public Entity(JSONObject obj) {
        type = obj.optString("type");
        relevance = obj.getDouble("relevance");
        sentiment = new Sentiment(obj.getJSONObject("sentiment"));
        count = obj.optInt("count");
        text = obj.optString("text");
        name = obj.optString("name");
        dbpedia = obj.optString("dbpedia");
    }


    public String getDbpedia() {
        return dbpedia;
    }

    public String getName() {
        return name;
    }

    public String getText() {
        return text;
    }

    public int getCount() {
        return count;
    }

    public Sentiment getSentiment() {
        return sentiment;
    }

    public double getRelevance() {
        return relevance;
    }

    public String getType() {
        return type;
    }
}
