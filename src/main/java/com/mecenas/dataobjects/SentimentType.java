package com.mecenas.dataobjects;

/**
 * Created by admin on 22/11/16.
 */
public enum SentimentType {
    POSITIVE, NEGATIVE, NEUTRAL
}
