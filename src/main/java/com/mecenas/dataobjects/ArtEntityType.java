package com.mecenas.dataobjects;

/**
 * Created by admin on 23/11/16.
 */
public enum ArtEntityType {
    ARTPIECE, ARTIST
}
