package com.mecenas;

import com.mecenas.dataobjects.ArtEntityType;
import com.mecenas.dataobjects.Source;
import com.mecenas.util.HttpHandler;
import sun.awt.image.ImageWatched;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by admin on 23/11/16.
 */
public class WebCrawlerReport {

    private ArtEntityType artEntityType;
    private String keyword;
    private final List<Source> sources;
    private HttpHandler handler;

    private HashMap<Source, WebCrawlerResult> crawlerMapping;

    public WebCrawlerReport(String keyword, List<Source> sources, ArtEntityType artEntityType, HttpHandler handler) {
        this.handler = handler;
        this.artEntityType = artEntityType;
        this.keyword = keyword;
        this.sources = sources;
        this.crawlerMapping = new HashMap<>();
        start();
    }

    private void start() {
        final WebCrawlerReport r = this;
        ArrayList<Thread> threads = new ArrayList<Thread>();
        for(final Source source: sources) {
            final String domain = source.getUrl();
            Thread t = new Thread(new Runnable() {
                public void run() {
                    WebCrawlerResult result = handler.getWebCrawlerResult(domain, keyword);
                    synchronized (r){
                        crawlerMapping.put(source, result);
                        r.notifyAll();
                    }
                }
            });
            threads.add(t);
            t.run();
        }
        try {
            for(Thread t : threads)
                t.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public HashMap<Source, WebCrawlerResult> getCrawlerMapping() {
        return crawlerMapping;
    }


}
