package com.mecenas;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.LinkedList;

/**
 * Created by admin on 22/11/16.
 */
public class WebCrawlerResult {

    private LinkedList<String> links;
    private String link;

    public WebCrawlerResult(String linksArray, String link){
        links = new LinkedList<String>();
        this.link = link;
        fromJSONString(linksArray);
    }


    public WebCrawlerResult(JSONArray jsonArray, String link){
        links = new LinkedList<String>();
        this.link = link;
        fromJSON(jsonArray);
    }



    public void fromJSONString(String array) {
        fromJSON(new JSONArray(array));
    }

    private void fromJSON(JSONArray jsonArray) {
        try {
            for(int i = 0; i < jsonArray.length(); i++){
                links.add(jsonArray.getString(i));
            }
        }
        catch(JSONException e){
            System.out.println(e.fillInStackTrace());
        }
    }


    public void addLink(String link) {
        links.add(link);
    }

    public int getLinksSize() {
        return links.size();
    }

    public LinkedList<String> getLinks(){
        return links;
    }


}
