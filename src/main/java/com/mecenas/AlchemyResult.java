package com.mecenas;

import com.mecenas.dataobjects.DocEmotions;
import com.mecenas.dataobjects.Entity;
import com.mecenas.dataobjects.Keyword;
import org.json.JSONArray;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;

/**
 * Created by admin on 22/11/16.
 */
public class AlchemyResult implements Comparable{

    private static final double titleSentimentRelevance = 0.8;
    private static final double normalSentimentRelevance = 0.5;


    private String searchKeword;

    private String title;
    private Date publicationDate;
    private DocEmotions docEmotions;
    private LinkedList<String> authors;
    private LinkedList<Entity> entities;
    private LinkedList<Keyword> keywords;


    private Entity desiredEntity;
    private double overallSentiment;

    private static SimpleDateFormat x = new SimpleDateFormat("yyyyMMdd'T'HHmmss");


    public AlchemyResult(String obj, String searchKeword) {
        this(new JSONObject(obj), searchKeword);
    }

    public AlchemyResult(JSONObject obj, String searchKeword) {
        this.authors = new LinkedList<String>();
        this.entities = new LinkedList<Entity>();
        this.keywords = new LinkedList<Keyword>();
        this.searchKeword = searchKeword;
        init(obj);

    }

    private void init(JSONObject obj){
        title = obj.optString("title");
        JSONObject publicationDate = obj.optJSONObject("publicationDate");
//        if(publicationDate != null /*&& publicationDate.optString("confident").equals("yes")*/)
        parseDate(publicationDate);
//        docEmotions = new DocEmotions(obj.optJSONObject("docEmotions"));
        JSONObject authorsObj = obj.optJSONObject("authors");
        if(authorsObj != null){
            JSONArray authors = authorsObj.getJSONArray("names");
            for(int i = 0; i < authors.length(); i++)
                this.authors.add(authors.getString(i));
        }
        JSONArray entities = obj.optJSONArray("entities");
        if(entities != null)
            for(int i = 0; i < entities.length(); i++)
                this.entities.add(new Entity(entities.getJSONObject(i)));

        JSONArray keywords = obj.optJSONArray("keywords");
        if(keywords != null)
            for(int i = 0; i < keywords.length(); i++)
                this.keywords.add(new Keyword(keywords.getJSONObject(i)));
    }

    private void parseDate(JSONObject publicationDate) {
        if(publicationDate != null){
            String date = publicationDate.optString("date");
            try {
                this.publicationDate = x.parse(date);
            } catch (ParseException e) {
                this.publicationDate = new Date();
            }
        }
        else
            this.publicationDate = new Date();

    }

    public double getMentionNumber(){
        return desiredEntity.getCount();
    }


    //Only call this function if hasRelatedEntity is true
    public double getOverallSentiment(){


        this.overallSentiment = desiredEntity.getSentiment().getScore();
        return this.overallSentiment;

    }

    private double getRelevance(){
        if(titleContainsKeyword())
            return titleSentimentRelevance;
        else
            return normalSentimentRelevance;
    }

    private boolean titleContainsKeyword(){
        if(title != null)
            if(this.title.toLowerCase().contains(searchKeword.toLowerCase()))
                return true;
        return false;
    }

    public boolean hasRelatedEntity(){
        if(getEntityFromKeywords() == null)
            return false;
        return true;
    }

    private Entity getEntityFromKeywords(){
        for(Entity e: entities) {
            if (e.getText().toLowerCase().contains(searchKeword.toLowerCase())) {
                this.desiredEntity = e;
                return e;
            }
        }
        return null;
    }

    public String getTitle() {
        return title;
    }

    public Date getPublicationDate() {
        return publicationDate;
    }

    public DocEmotions getDocEmotions() {
        return docEmotions;
    }

    public int compareTo(Object r) {
        if(publicationDate != null && ((AlchemyResult)r).getPublicationDate() != null)
            return getPublicationDate().compareTo(((AlchemyResult)r).getPublicationDate());
        else
            return 1;
    }

}
