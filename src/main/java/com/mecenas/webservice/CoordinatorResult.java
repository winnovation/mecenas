package com.mecenas.webservice;

import com.mecenas.Coordinator;

/**
 * Created by admin on 06/12/16.
 */
public class CoordinatorResult {

    private final int firstTermMentionNumber;
    private final int secondTermMentionNumber;
    private final int thirdTermMentionNumber;
    private final int fourthTermMentionNumber;

    private final double firstTermSentimentNumber;
    private final double secondTermSentimentNumber;
    private final double thirdTermSentimentNumber;
    private final double fourthTermSentimentNumber;

    private final double shortTermMentionGrow;
    private final double midTermMentionGrow;
    private final double longMidTermMentionGrow;

    private final double shortTermSentimentGrow;
    private final double midTermSentimentGrow;

    private final double longMidTermSentimentGrow;

    private final Coordinator.DateMargin dateMargin;

    public CoordinatorResult(Coordinator coordinator){
        this.firstTermMentionNumber = coordinator.getFirstTermMentionNumber();
        this.secondTermMentionNumber = coordinator.getSecondTermMentionNumber();
        this.thirdTermMentionNumber= coordinator.getThirdTermMentionNumber();
        this.fourthTermMentionNumber= coordinator.getFourthTermMentionNumber();

        this.firstTermSentimentNumber = coordinator.getFirstTermSentimentValue();
        this.secondTermSentimentNumber = coordinator.getSecondTermSentimentValue();
        this.thirdTermSentimentNumber = coordinator.getThirdTermSentimentValue();
        this.fourthTermSentimentNumber = coordinator.getFourthTermSentimentValue();

        this.shortTermMentionGrow = coordinator.getShortTermMentionGrow();
        this.midTermMentionGrow = coordinator.getMidTermMentionGrow();
        this.longMidTermMentionGrow = coordinator.getLongMidTermMentionGrow();

        this.shortTermSentimentGrow = coordinator.getShortTermSentimentGrow();
        this.midTermSentimentGrow = coordinator.getMidTermSentimentGrow();
        this.longMidTermSentimentGrow = coordinator.getLongMidTermSentimentGrow();

        this.dateMargin = coordinator.getDateMargin();
    }

    public int getFirstTermMentionNumber() {
        return firstTermMentionNumber;
    }

    public int getSecondTermMentionNumber() {
        return secondTermMentionNumber;
    }

    public int getThirdTermMentionNumber() {
        return thirdTermMentionNumber;
    }

    public int getFourthTermMentionNumber() {
        return fourthTermMentionNumber;
    }

    public double getFirstTermSentimentNumber() {
        return firstTermSentimentNumber;
    }

    public double getSecondTermSentimentNumber() {
        return secondTermSentimentNumber;
    }

    public double getThirdTermSentimentNumber() {
        return thirdTermSentimentNumber;
    }

    public double getFourthTermSentimentNumber() {
        return fourthTermSentimentNumber;
    }

    public double getShortTermMentionGrow() {
        return shortTermMentionGrow;
    }

    public double getMidTermMentionGrow() {
        return midTermMentionGrow;
    }

    public double getLongMidTermMentionGrow() {
        return longMidTermMentionGrow;
    }

    public double getShortTermSentimentGrow() {
        return shortTermSentimentGrow;
    }

    public double getMidTermSentimentGrow() {
        return midTermSentimentGrow;
    }

    public double getLongMidTermSentimentGrow() {
        return longMidTermSentimentGrow;
    }

    public Coordinator.DateMargin getDateMargin() {
        return dateMargin;
    }


}
