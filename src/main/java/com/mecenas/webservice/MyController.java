package com.mecenas.webservice;

import com.mecenas.Coordinator;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.concurrent.atomic.AtomicLong;


/**
 * Created by admin on 06/12/16.
 */
@Controller
public class MyController {

    @RequestMapping(value="/trend", method= RequestMethod.GET, produces= {"application/json"})
    public @ResponseBody CoordinatorResult trend(@RequestParam(value="keyword") String keyword) {
        Coordinator c = new Coordinator(keyword);
        c.start();
        return new CoordinatorResult(c);
    }

}
