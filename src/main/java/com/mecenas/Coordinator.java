package com.mecenas;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.mecenas.dataobjects.ArtEntityType;
import com.mecenas.dataobjects.Source;
import com.mecenas.util.HttpHandler;
import com.mecenas.util.db.DBAccess;

import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

/**
 * Created by admin on 04/12/16.
 */
public class Coordinator {


    private DBAccess db;
    private HttpHandler httpHandler;
    private String keyword;
    private HashMap<Source, AlchemyReport> alchemyReportMapping;
    private GoogleTrend googleTrend;

    private DateMargin dateMargin;

    private ArrayList<Source> sources;
    private ArrayList<Thread> threads = new ArrayList<>();
    public Coordinator(String keyword){
        db = new DBAccess();
        db.connect();
        this.keyword = keyword;
        httpHandler = new HttpHandler();
        alchemyReportMapping = new HashMap<>();
    }


    public void start(){
        sources = db.getSources();
        solveAlchemyReportMapping();

        try {
            for(Thread t : threads)
                t.join();
            orderAlchemyReportsDateMargins();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private void solveAlchemyReportMapping(){
        final WebCrawlerReport crawlerReport = new WebCrawlerReport(keyword, this.sources, ArtEntityType.ARTIST, httpHandler);
        final Coordinator coordinator = this;
        for(final Source s : crawlerReport.getCrawlerMapping().keySet()){
            Thread t = new Thread(new Runnable() {
                public void run() {
                    AlchemyReport alchemyReport = new AlchemyReport(crawlerReport.getCrawlerMapping().get(s).getLinks(), keyword);
                    synchronized (coordinator) {
                        alchemyReportMapping.put(s, alchemyReport);
                    }
                }
            });
            this.threads.add(t);
            t.run();
        }
    }

    private void orderAlchemyReportsDateMargins() {
        Date firstDate = new Date();
        Date lastDate = new Date();
        for(Source s: alchemyReportMapping.keySet()){
            AlchemyReport r = alchemyReportMapping.get(s);
            Date fd = r.getFirstValidDate();
            if(fd.before(firstDate))
                firstDate = fd;
        }
        long timeQuarter = (lastDate.getTime() - firstDate.getTime()) / 4;
        Date longTerm = firstDate;
        Date longMidTerm = new Date(firstDate.getTime() + timeQuarter);
        Date midTerm = new Date(firstDate.getTime() + 2*timeQuarter);
        Date shortTerm = new Date(firstDate.getTime() + 3*timeQuarter);
        this.dateMargin = new DateMargin(longTerm, longMidTerm, midTerm, shortTerm);
        for(Source s: alchemyReportMapping.keySet()){
            alchemyReportMapping.get(s).sortResultsTerms(dateMargin);
        }

    }

    private void solveGoogleTrendMapping(){
        final Coordinator coordinator = this;
        Thread t = new Thread(new Runnable() {
            public void run() {
                googleTrend = httpHandler.getGoogleTrendResult(keyword);
            }
        });
        this.threads.add(t);
        t.run();
    }

    public int getFirstTermMentionNumber(){
        int mentionNumber = 0;
        for(Source s: sources){
            mentionNumber += alchemyReportMapping.get(s).getFirstTermMentionNumber();
        }
        return mentionNumber;
    }

    public int getSecondTermMentionNumber(){
        int mentionNumber = 0;
        for(Source s: sources){
            mentionNumber += alchemyReportMapping.get(s).getSecondTermMentionNumber();
        }
        return mentionNumber;
    }
    public int getThirdTermMentionNumber(){
        int mentionNumber = 0;
        for(Source s: sources){
            mentionNumber += alchemyReportMapping.get(s).getThirdTermMentionNumber();
        }
        return mentionNumber;
    }
    public int getFourthTermMentionNumber(){
        int mentionNumber = 0;
        for(Source s: sources){
            mentionNumber += alchemyReportMapping.get(s).getFourthTermMentionNumber();
        }
        return mentionNumber;
    }
    public double getFirstTermSentimentValue(){
        double sourceInfluenceSum = 0;
        double weightedInfluenceSum = 0;
        for(Source s: sources){
            sourceInfluenceSum += s.getInfluence();
            double sentimentValue = alchemyReportMapping.get(s).getFirstTermSentimentValue();
            weightedInfluenceSum += (sentimentValue * s.getInfluence());
        }
        if(weightedInfluenceSum == 0)
            return 0;
        return weightedInfluenceSum/sourceInfluenceSum;
    }

    public double getSecondTermSentimentValue(){
        double sourceInfluenceSum = 0;
        double weightedSentimentSum = 0;
        for(Source s: sources){
            sourceInfluenceSum += s.getInfluence();
            weightedSentimentSum += alchemyReportMapping.get(s).getSecondTermSentimentNumber() * s.getInfluence();
        }
        if(weightedSentimentSum == 0)
            return 0;
        return weightedSentimentSum/sourceInfluenceSum;
    }
    public double getThirdTermSentimentValue(){
        double sourceInfluenceSum = 0;
        double weightedSentimentSum = 0;
        for(Source s: sources){
            sourceInfluenceSum += s.getInfluence();
            weightedSentimentSum += alchemyReportMapping.get(s).getThirdTermSentimentNumber() * s.getInfluence();
        }
        if(weightedSentimentSum == 0)
            return 0;
        return weightedSentimentSum/sourceInfluenceSum;
    }
    public double getFourthTermSentimentValue(){
        double sourceInfluenceSum = 0;
        double weightedSentimentSum = 0;
        for(Source s: sources){
            sourceInfluenceSum += s.getInfluence();
            weightedSentimentSum += alchemyReportMapping.get(s).getFourthTermSentimentNumber() * s.getInfluence();
        }
        if(weightedSentimentSum == 0)
            return 0;
        return weightedSentimentSum/sourceInfluenceSum;
    }
    public double getShortTermMentionGrow(){
        int firstTermMention = getFirstTermMentionNumber();
        int secondTermMention = getSecondTermMentionNumber();
        int thirdTermMention = getThirdTermMentionNumber();
        int fourthTermMention = getFourthTermMentionNumber();

        int sum = firstTermMention + secondTermMention + thirdTermMention;
        double average = (double) sum / 3.0;
        return ((fourthTermMention - average) / average) * 100;
    }

    public double getMidTermMentionGrow(){
        int firstTermMention = getFirstTermMentionNumber();
        int secondTermMention = getSecondTermMentionNumber();
        int thirdTermMention = getThirdTermMentionNumber();
        int fourthTermMention = getFourthTermMentionNumber();

        int sum = firstTermMention + secondTermMention;
        double average = (double) sum / 2.0;
        return ((((thirdTermMention + fourthTermMention)/2.0) - average) / average) * 100;
    }
    public double getLongMidTermMentionGrow(){
        int firstTermMention = getFirstTermMentionNumber();
        int secondTermMention = getSecondTermMentionNumber();
        int thirdTermMention = getThirdTermMentionNumber();
        int fourthTermMention = getFourthTermMentionNumber();

        return ((((secondTermMention + thirdTermMention + fourthTermMention)/3.0) - firstTermMention) / firstTermMention) * 100;
    }
    public double getShortTermSentimentGrow(){
        double firstTermSentiment = getFirstTermSentimentValue();
        double secondTermSentiment = getSecondTermSentimentValue();
        double thirdTermSentiment = getThirdTermSentimentValue();
        double fourthTermSentiment = getFourthTermSentimentValue();

        double sum = firstTermSentiment + secondTermSentiment + thirdTermSentiment;
        System.out.println("Sum: " + sum);
        double average = sum / 3.0;
        System.out.println("Average: " + average);
        System.out.println(fourthTermSentiment - average);
        System.out.println(((fourthTermSentiment - average) / Math.abs(average)));
        return ((fourthTermSentiment - average) / Math.abs(average)) * 100;

    }

    public double getMidTermSentimentGrow(){
        double firstTermSentiment = getFirstTermSentimentValue();
        double secondTermSentiment = getSecondTermSentimentValue();
        double thirdTermSentiment = getThirdTermSentimentValue();
        double fourthTermSentiment = getFourthTermSentimentValue();

        double sum = firstTermSentiment + secondTermSentiment;
        double average =  sum / 2.0;
        return ((((thirdTermSentiment + fourthTermSentiment)/2.0) - average) / Math.abs(average)) * 100;
    }
    public double getLongMidTermSentimentGrow(){
        double firstTermSentiment = getFirstTermSentimentValue();
        double secondTermSentiment = getSecondTermSentimentValue();
        double thirdTermSentiment = getThirdTermSentimentValue();
        double fourthTermSentiment = getFourthTermSentimentValue();

        return ((((secondTermSentiment + thirdTermSentiment + fourthTermSentiment)/3.0) - firstTermSentiment) / Math.abs(firstTermSentiment)) * 100;
    }
    public DateMargin getDateMargin() {
        return dateMargin;
    }

    public HashMap<Source, AlchemyReport> getAlchemyReportMapping(){
        return alchemyReportMapping;
    }

    public class DateMargin {
        @JsonIgnore
        private Date longTerm;
        private final String longTermDate;
        @JsonIgnore
        private Date longMidTerm;
        private final String longMidTermDate;
        @JsonIgnore
        private Date midTerm;
        private final String midTermDate;

        @JsonIgnore
        private Date shortTerm;
        private final String shortTermDate;

        public DateMargin(Date longTerm, Date longMidTerm, Date midTerm, Date shortTerm) {
            Format formatter = new SimpleDateFormat("dd-MM-yyyy");
            this.longTerm = longTerm;
            this.longTermDate = formatter.format(longTerm);

            this.longMidTerm = longMidTerm;
            this.longMidTermDate = formatter.format(longMidTerm);

            this.midTerm = midTerm;
            this.midTermDate = formatter.format(midTerm);

            this.shortTerm = shortTerm;
            this.shortTermDate = formatter.format(shortTerm);
        }

        public Date getLongTerm() {
            return longTerm;
        }

        public Date getLongMidTerm() {
            return longMidTerm;
        }

        public Date getMidTerm() {
            return midTerm;
        }

        public Date getShortTerm() {
            return shortTerm;
        }

        public String getLongTermDate() {
            return longTermDate;
        }

        public String getLongMidTermDate() {
            return longMidTermDate;
        }

        public String getMidTermDate() {
            return midTermDate;
        }

        public String getShortTermDate() {
            return shortTermDate;
        }
    }
}
